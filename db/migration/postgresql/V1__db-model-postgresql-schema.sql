CREATE TABLE bds.address (
    id_address bigserial NOT NULL,
    city character varying(45) NOT NULL,
    house_number integer NOT NULL,
    street character varying(45) NOT NULL,
    zip_code character varying(45),
    PRIMARY KEY (id_address)
);

ALTER TABLE bds.address OWNER TO sedaq;

CREATE TABLE bds.person (
    id_person bigserial NOT NULL,
    birthdate date,
    email character varying(100) NOT NULL,
    given_name character varying(45) NOT NULL,
    family_name character varying(45) NOT NULL,
    nickname character varying(45) NOT NULL,
    pwd character varying(255) NOT NULL,
    id_address bigint,
    PRIMARY KEY (id_person),
    CONSTRAINT fksukp9r53qgth8ex065xwuw86t
        FOREIGN KEY (id_address)
        REFERENCES bds.address(id_address)
);

ALTER TABLE bds.person OWNER TO sedaq;

CREATE TABLE bds.contact_type (
    id_contact_type serial NOT NULL,
    title character varying(45) NOT NULL,
    PRIMARY KEY (id_contact_type),
    UNIQUE (title)
);

ALTER TABLE bds.contact_type OWNER TO sedaq;

CREATE TABLE bds.contact (
    id_contact bigserial NOT NULL,
    contact character varying(45) NOT NULL,
    id_contact_type bigint,
    id_person bigint,
    PRIMARY KEY (id_contact),
    CONSTRAINT fksdofi6j6flua1it19rnx6xcvb 
        FOREIGN KEY (id_person)
        REFERENCES bds.person(id_person),
    CONSTRAINT fkx0wbmi3et03b7xwys8sa0d7
        FOREIGN KEY (id_contact_type)
        REFERENCES bds.contact_type(id_contact_type)
);

ALTER TABLE bds.contact OWNER TO sedaq;

CREATE TABLE bds.meeting (
    id_meeting bigserial NOT NULL,
    note character varying(1000),
    place character varying(200) NOT NULL,
    start_time timestamp without time zone NOT NULL,
    end_time timestamp without time zone NOT NULL,
    PRIMARY KEY (id_meeting)
);

ALTER TABLE bds.meeting OWNER TO sedaq;

CREATE TABLE bds.person_has_meeting (
    id_person bigint NOT NULL,
    id_meeting bigint NOT NULL,
    is_organizer boolean NOT NULL,
    PRIMARY KEY (id_person, id_meeting),
    CONSTRAINT fk92pqvoxbjila4o02fp18rstq4 
        FOREIGN KEY (id_person)
        REFERENCES bds.person(id_person),
    CONSTRAINT fk92pqvoxbjila4o02fp18rstq5
        FOREIGN KEY (id_meeting)
        REFERENCES bds.meeting(id_meeting)
);

ALTER TABLE bds.person_has_meeting OWNER TO sedaq;

CREATE TABLE bds.role (
    id_role SERIAL NOT NULL,
    role VARCHAR(45) NOT NULL,
    PRIMARY KEY (id_role)
);

ALTER TABLE bds.role OWNER TO sedaq;

CREATE TABLE bds.person_has_role (
    id_person bigserial NOT NULL,
    id_role bigserial NOT NULL,
    started_at TIMESTAMP NOT NULL,
    ended_at TIMESTAMP,
    expiration_date TIMESTAMP,
    PRIMARY KEY (id_role, id_person),
    CONSTRAINT fk_person_has_role_person1
        FOREIGN KEY (id_person)
        REFERENCES bds.person (id_person)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fk_person_has_role_role1
        FOREIGN KEY (id_role)
        REFERENCES bds.role (id_role)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION
);

ALTER TABLE bds.person_has_role OWNER TO sedaq;

CREATE TABLE bds.relationship_type (
    id_relationship_type bigserial NOT NULL,
    title character varying(255) NOT NULL,
    PRIMARY KEY (id_relationship_type),
    UNIQUE (title)
);

ALTER TABLE bds.relationship_type OWNER TO sedaq;

CREATE TABLE bds.relationship (
    id_relationship bigserial NOT NULL,
    note character varying(200),
    id_person1 bigint,
    id_person2 bigint,
    id_relationship_type bigint,
    PRIMARY KEY (id_relationship),
    CONSTRAINT fk3b7pcnfgfdblhublf8t8dvn9l
        FOREIGN KEY (id_relationship_type)
        REFERENCES bds.relationship_type(id_relationship_type)
        ON DELETE NO ACTION
        ON UPDATE NO ACTION,
    CONSTRAINT fkahw6wujjfkm21yqfhar09k3fk
        FOREIGN KEY (id_person1) 
        REFERENCES bds.person(id_person),
    CONSTRAINT fkdkyv2jjonl0trwvbsnmw7wugr
        FOREIGN KEY (id_person2)
        REFERENCES bds.person(id_person)
);

ALTER TABLE bds.relationship OWNER TO sedaq;
