# Dummy Deployment

Clone the repository and run the following command in the project root directory to run the services.

```shell
$ docker-compose up
```
